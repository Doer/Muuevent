-- -----------------------------
-- 表结构 `muucmf_muuevent`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `muucmf_muuevent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '发起人',
  `title` varchar(255) NOT NULL COMMENT '活动名称',
  `description` varchar(255) NOT NULL COMMENT '活动摘要',
  `explain` text NOT NULL COMMENT '详细内容',
  `sTime` int(11) NOT NULL COMMENT '活动开始时间',
  `eTime` int(11) NOT NULL COMMENT '活动结束时间',
  `address` varchar(255) NOT NULL COMMENT '活动地点',
  `province` int(11) NOT NULL COMMENT '省份',
  `city` int(11) NOT NULL COMMENT '城市',
  `district` int(11) NOT NULL COMMENT '区、县、镇',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `limitCount` int(11) NOT NULL COMMENT '限制人数',
  `cover_id` int(11) NOT NULL COMMENT '封面ID',
  `attentionCount` int(11) NOT NULL COMMENT '在线报名人数',
  `status` int(11) NOT NULL,
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `view_count` int(11) NOT NULL COMMENT '浏览量',
  `reply_count` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `signCount` int(11) NOT NULL COMMENT '报名人数',
  `is_recommend` tinyint(4) NOT NULL COMMENT '是否推荐',
  `point` varchar(255) NOT NULL COMMENT '定位点{lng,lat}',
  `reason` varchar(255) NOT NULL COMMENT '审核失败原因',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;


-- -----------------------------
-- 表结构 `muucmf_muuevent_attend`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `muucmf_muuevent_attend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `create_time` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0为报名，1为参加',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;


-- -----------------------------
-- 表结构 `muucmf_muuevent_type`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `muucmf_muuevent_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `allow_post` tinyint(4) NOT NULL,
  `pid` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;


-- -----------------------------
-- 表内记录 `muucmf_muuevent_type`
-- -----------------------------
INSERT INTO `muucmf_muuevent_type` VALUES ('1', '生活', '1403859500', '1527787813', '1', '0', '0', '2');
INSERT INTO `muucmf_muuevent_type` VALUES ('2', '官方活动', '1527781778', '1527781767', '1', '0', '0', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('3', '亲子', '1527781793', '1527787818', '1', '0', '0', '3');
INSERT INTO `muucmf_muuevent_type` VALUES ('4', '行业', '1527781812', '1527787808', '1', '0', '0', '1');
INSERT INTO `muucmf_muuevent_type` VALUES ('5', '学习', '1527782172', '1527787826', '1', '0', '0', '4');
INSERT INTO `muucmf_muuevent_type` VALUES ('6', 'IT互联网', '1527782343', '0', '1', '0', '4', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('7', '课程', '1527783787', '0', '1', '0', '5', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('8', '读书', '1527787229', '0', '1', '0', '5', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('9', '职场', '1527787240', '0', '1', '0', '5', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('10', '社团', '1527787251', '0', '1', '0', '5', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('11', '讲座', '1527787260', '0', '1', '0', '5', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('12', '创业', '1527821329', '0', '1', '0', '4', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('13', '科技', '1527821344', '0', '1', '0', '4', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('14', '金融', '1527821355', '0', '1', '0', '4', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('15', '游戏', '1527821362', '0', '1', '0', '4', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('16', '文娱', '1527821376', '0', '1', '0', '4', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('17', '电商', '1527821383', '0', '1', '0', '4', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('18', '教育', '1527821393', '0', '1', '0', '4', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('19', '营销', '1527821412', '0', '1', '0', '4', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('20', '设计', '1527821419', '0', '1', '0', '4', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('21', '地产', '1527821426', '0', '1', '0', '4', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('22', '医疗', '1527821434', '0', '1', '0', '4', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('23', '服务业', '1527821441', '0', '1', '0', '4', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('24', '演出', '1528027615', '0', '1', '0', '1', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('25', '公益', '1528027633', '0', '1', '0', '1', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('26', '户外出行', '1528027644', '0', '1', '0', '1', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('27', '聚会交友', '1528027669', '0', '1', '0', '1', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('28', '时尚', '1528027685', '0', '1', '0', '1', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('29', '体育赛事', '1528027696', '0', '1', '0', '1', '0');
INSERT INTO `muucmf_muuevent_type` VALUES ('30', '运动健康', '1528027758', '0', '1', '0', '1', '0');
